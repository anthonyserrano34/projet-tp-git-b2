<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Shop Homepage - Start Bootstrap Template</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="index.php">
                    <img src='css/logo.png'>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <li class="nav-item"><a class="nav-link active" aria-current="page" href="index.php">Accueil</a></li>
                    </ul>
                    <!-- <form class="d-flex">
                        <button class="btn btn-outline-dark" type="submit">
                            <i class="bi bi-card-list"></i>
                            Ajouter une recette
                            <span class="badge bg-dark text-white ms-1 rounded-pill">+</span>
                        </button>
                    </form>
                    <form class="d-flex">
                        <button class="btn btn-outline-dark" type="submit">
                            <i class="bi bi-egg-fried"></i>
                            Ajouter un ingrédient
                            <span class="badge bg-dark text-white ms-1 rounded-pill">+</span>
                        </button>
                    </form> -->
                </div>
            </div>
        </nav>
        <header class="bg-green py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="text-center text-white">
                    <h1 class="display-4 fw-bolder">Liste des recettes</h1>
                    <p class="lead fw-normal text-white-50 mb-0">Bon appétit !</p>
                </div>
            </div>
        </header>
        <section class="py-5">
            <div class="container px-4 px-lg-5 mt-5">
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                <?php
                $bdd = new PDO('mysql:host=localhost;dbname=recettes', 'root', '');
                $recettes = $bdd -> query ('SELECT * FROM recette');
                foreach($recettes as $recette){
                ?>
                    <div class="col mb-5">
                        <div class="card h-100">
                            <img class="card-img-top" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." />
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <h5 class="fw-bolder">
                                        <?php 
                                            echo $recette['nom']
                                        ?>
                                        <hr>
                                    <h5>
                                    <?php 
                                        echo $recette['temps']
                                    ?>
                                    <hr>
                                    <h5>Ingrédients</h5>
                                           - 50g de chocolat
                                           - 100ml de lait
                                           - 3 cuillières à soupe de farine
                                           <hr>
                                           <h5>Recette</h5>
                                            <?php 
                                                echo $recette['descript']
                                            ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    ?>

                </div>
            </div>
        </section>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
